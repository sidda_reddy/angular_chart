import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class WeatherService {

  constructor(private http: HttpClient) { }
    
    weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=6e393de18c248d3771bed38a11f0b2b5';
    getWeather() {
        return this.http.get(this.weatherUrl).map(result => result);
        }
}
