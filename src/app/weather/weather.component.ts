import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Chart } from 'chart.js';
import 'rxjs/add/operator/map';

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

    constructor(private weatherService: WeatherService) { }
    public title = 'App Title';
    chart = []; // This will hold our chart info
    public weather;
    ngOnInit() {
        this.loadWeatherData();
    }
    loadWeatherData() {
        this.weatherService.getWeather()
            .subscribe(res => {
                this.weather = res;
                console.log(res);
                //let temp_max = res.main.temp_max;
                //let temp_min = res.main.temp_min;
                let alldates = res.dt;
                //alert(temp_max);
                let weatherDates = [];
                let temp_max = [];
                let temp_min = [];
                
                for (let i = 0; i <= 10; i++) {
                    let jsdate = new Date((alldates * i) * 1000)
                    if(i%2==0) {
                         temp_max.push(res.main.temp_max-i);
                          temp_min.push(res.main.temp_min+i);
                        } else {
                             temp_max.push(res.main.temp_max+i);
                             temp_min.push(res.main.temp_min-(i+i));
                        }
                   
                    weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }))
                }
                
                this.chart = new Chart('canvas', {
                    type: 'line',
                    data: {
                        labels: weatherDates,
                        datasets: [
                            {
                                data: temp_max,
                                borderColor: "#3cba9f",
                                fill: false
                            },
                            {
                                data: temp_min,
                                borderColor: "#ffcc00",
                                fill: false
                            },
                        ]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                display: true
                            }],
                            yAxes: [{
                                display: true
                            }],
                        }
                    }
                });
                /*alldates.forEach((res) => {
                    let jsdate = new Date(res * 1000)
                    weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }))
                }) */
            });
    }

}
